// JavaScript Document

// Wait for PhoneGap to load
document.addEventListener("deviceready", onDeviceReady, false);

// PhoneGap is ready
function onDeviceReady() {
}

function getZips(){
    app.showLoading();
    var zipCodes = [];
    if(localStorage.zips){
        zipCodes = localStorage.zips.split(',');
        zipCodes.sort();
    }
    else
    {
        
        zipCodes[0] = "45315";
        zipCodes[1] = "33302";
        localStorage.zips = zips.join(',');
    }
    zips = []; 
    $.each(zipCodes, function(i, z){
        $.ajax({
            url: "http://free.worldweatheronline.com/feed/weather.ashx?q=" + z + "&format=json&num_of_days=2&key=6255032fcc200126121812",
            dataType: "jsonp",
            async: false,
            success: function( data ){
                $.each(data.data, function(i, item){
                    if( i ==='current_condition' ){
                        $.each(item, function(ii, currentcondition){
                            var cc = {
                                        zip: z,
                                        cloudcover: currentcondition.cloudcover,
                                        temp_C: currentcondition.temp_C,
                                        temp_F: currentcondition.temp_F
                                };
                                cc.weatherIconUrl = '';
                                $.each(currentcondition.weatherIconUrl, function(wii, wi){
                                    cc.weatherIconUrl = wi.value;
                                });
        
                                zips.push( cc ); 
                        });
                    }
                });
                zips.sort(SortByZip);
                showZips();
            } 
        })
    });
}

function viewZips(){
    var listView = $("#weatherZips").data("kendoMobileListView");
    listView.refresh();
}

function showZips(){ 
    var listView = $("#weatherZips").data("kendoMobileListView");
    listView.destroy();
    
    var template1 = kendo.template($("#homeTemplate").text()); 
    $("#weatherZips").kendoMobileListView({ 
        dataSource:zips,
        template: template1,
        pullToRefresh: true,
        pullParameters: function(item) {
            getZips();
        },
        refreshTemplate: "Reloading...",
        click: function(e){
            console.log('herer');
            var zip = e.target.data("id");
            if(zip){
                getDetails(zip);
            }
        }
    });
}

function getDetails(zip){
    app.showLoading();
    var weatherData = new kendo.data.DataSource({
                            transport: {
                                read: {
                                    url: "http://free.worldweatheronline.com/feed/weather.ashx?q=" + zip + "&format=json&num_of_days=2&key=6255032fcc200126121812",
                                    dataType: "jsonp"
                                }
                            },
                            schema: {
                                data: function(response) {
                                    
                                    current = [];
                                    weather = [];
                                    
                                    $.each(response.data, function(i, item){
                                        if( i ==='current_condition' ){
                                            $.each(item, function(ii, currentcondition){
                                                var cc = {
                                                        zip: zip,
                                                        cloudcover: currentcondition.cloudcover, 
                                                        humidity: currentcondition.humidity,
                                                        observation_time: currentcondition.observation_time,
                                                        precipMM: currentcondition.precipMM,
                                                        pressure: currentcondition.pressure,
                                                        temp_C: currentcondition.temp_C,
                                                        temp_F: currentcondition.temp_F,
                                                        visibility: currentcondition.visibility,
                                                        winddir16Point: currentcondition.winddir16Point,
                                                        windspeedMiles: currentcondition.windspeedMiles
                                                    };

                                                    cc.weatherDesc = '';
                                                    cc.weatherIconUrl = '';
                                                    $.each(currentcondition.weatherDesc, function(wdi, wd){
                                                        cc.weatherDesc = wd.value;
                                                    });
                                                    $.each(currentcondition.weatherIconUrl, function(wii, wi){
                                                        cc.weatherIconUrl = wi.value;
                                                    });
                                                    current.push(cc); 
                                            });
                                        }
                                        if( i === 'weather' ) {
                                            $.each( item, function( ii, weatherForecast ){
                                                var wf = {
                                                        date:  kendo.toString(kendo.parseDate(weatherForecast.date), "MM/dd/yyyy"),
                                                        precipMM: weatherForecast.precipMM,
                                                        tempMaxC: weatherForecast.tempMaxC,
                                                        tempMaxF: weatherForecast.tempMaxF,
                                                        tempMinC: weatherForecast.tempMinC,
                                                        tempMinF: weatherForecast.tempMinF,
                                                        winddirection: weatherForecast.winddirection,
                                                        windspeedMiles: weatherForecast.windspeedMiles
                                                    };
                                                wf.weatherDesc = '';
                                                wf.weatherIconUrl = '';
                                                $.each(weatherForecast.weatherDesc, function(wdi, wd){
                                                    wf.weatherDesc = wd.value;
                                                });
                                                $.each(weatherForecast.weatherIconUrl, function(wii, wi){
                                                    wf.weatherIconUrl = wi.value;
                                                });
                                                weather.push(wf);
                                            });
                                        }
                                    });
                                        
                                    app.navigate("#tabstrip-details");
    
                                    var templateCC = kendo.template($("#ccTemplate").text()); 
                                    var templateForecast = kendo.template($("#forecastTemplate").text()); 
                                    
                                    $("#weatherCC").kendoMobileListView({ dataSource: current, template:templateCC });
                                    $("#weatherForecast").kendoMobileListView({ dataSource: weather, template:templateForecast });   
                                    return response.data;
                                }
                            }
                });
    weatherData.fetch();
}

function getSettings(){
    app.showLoading();
    var listView = $("#savedZips").data("kendoMobileListView");

    // detach events
    listView.destroy();
    
    var zips = [];
    if(localStorage.zips){
        zips = localStorage.zips.split(',');
    }
    else
    {
        zips = [];
        zips[0] = "45315";
        zips[1] = "33302";
        localStorage.zips = zips.join(',');
    }
    savedZips = zips;
    var template1 = kendo.template($("#settingsTemplate").text()); 
    $("#savedZips").kendoMobileListView({ 
        dataSource:savedZips,
        template:template1,
        click: function(e) {
         removeZip(e.target.data("id"));
        }
    });
}

function removeZip(zip){
    app.showLoading();
    var zips = [];    
    if(localStorage.zips){
        zips = localStorage.zips.split(',');
    }
    zips = jQuery.grep(zips, function(value) {
        return value != zip;
    });
    localStorage.zips = zips.join(',');
    getZips();
    viewZips();
    getSettings();
}

function addZip(){
   $('#zipCode').val(''); 
   app.navigate("tabstrip-addZip"); 
}

function saveZip(){
    app.showLoading();
    var pat=/^[0-9]{5}$/;
    if(!pat.test($('#zipCode').val())){
        $("#errorSpan").show();
        app.hideLoading();
    }
    else {
        $("#errorSpan").hide();
        var zips = [];    
        if(localStorage.zips){
            zips = localStorage.zips.split(',');
        }
        var newZip = $("#zipCode").val();
        zips.push(newZip);
        localStorage.zips = zips.sort().join(',');
        getSettings();
        getZips();
        viewZips();
        app.navigate("#tabstrip-settings");
    }
}

function SortByZip(a, b){
  var aZip = a.zip;
  var bZip = b.zip; 
  return ((aZip < bZip) ? -1 : ((aZip > bZip) ? 1 : 0));
}